### running
- git clone
- npm update
- gulp build
- npm start

### prereq
- create a config.json inside private, follow [private readme](private/readme.md) for this
- create a GITHUB_WEBHOOK_SECRET environment var with your secret
