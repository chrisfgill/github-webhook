var gulp = require('gulp');
var ts = require('gulp-typescript');
var clean = require('gulp-clean');


gulp.task('default', ['build'], function() {
    gulp.watch('src/*.ts', ['typescript']);
});

gulp.task('typescript', function() {
    var tsProject = ts.createProject('tsconfig.json');
    tsProject.config.exclude.push("client");
    var tsResult = tsProject.src()
		.pipe(tsProject());
	
	return tsResult.js.pipe(gulp.dest('build'));
});

gulp.task('clean', function() {
    return gulp.src('build/', {read: false})
        .pipe(clean());
});

gulp.task('build', ['clean', 'typescript']);
