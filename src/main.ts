import * as express from 'express';
import * as http from 'http';
import * as body from 'body-parser';
import {createHmac} from 'crypto';

const config = require('../private/config.json'); // private smtp info
import {Mail,Mailer} from './mailer';
let mailer = new Mailer(config.mail);

let app = express();
let server = (http as any).Server(app); //ignores ts errors!
server.listen(6060);

const secret = process.env.GITHUB_WEBHOOK_SECRET;

if (!secret) { throw(function() { console.error('missing environment vars: GITHUB_WEBHOOK_SECRET')} )}


app.post('/github', body.json(), function (req, res) {
    let sig = function (body,secret) {
        return "sha1=" + createHmac('sha1', secret)
            .update(body)
            .digest('hex');
    }

    if (sig(JSON.stringify(req.body),secret) ==
        req.headers['x-hub-signature']) {
        res.sendStatus(200);

        console.log(req.headers['x-github-event']);

        if (req.headers['x-github-event'] == 'push')
        { on_push(req.body) }
        
        else if ((req.headers['x-github-event'] == 'team_add') ||
                 (req.headers['x-github-event'] == 'member') ||
                 (req.headers['x-github-event'] == 'membership'))
        { on_team(req.body) }
        
        else if ((req.headers['x-github-event'] == 'repository') ||
                 (req.headers['x-github-event'] == 'public'))
        { on_repo(req.body) }
    }
    else { res.sendStatus(400) }
});


function on_push(body) {
    console.log('push', body.pusher, body.repository.full_name);
    mailer.send(new Mail('push', '<h5>new push</h5> <br>'
                         + body.pusher.email
                         + '<br>for<br>'
                         + body.repository.full_name));
}

function on_team(body) {
    console.log('team', JSON.stringify(body,null,2));
    mailer.send(new Mail('team', '<h5>team modification</h5> <br>'
                         + JSON.stringify(body,null,2)));}

function on_repo(body) {
    console.log('repo', JSON.stringify(body,null,2));
    mailer.send(new Mail('repo', '<h5>repo modification</h5> <br>'
                         + JSON.stringify(body,null,2)));
}
