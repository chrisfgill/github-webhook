const mail = require('nodemailer');

export class Mail {
    kind: string
    body: string

    constructor (kind:string,body:string) {
        this.kind = kind;
        this.body = body;
    }
}

export class Mailer {
    private conn
    to:string
    from:string

    constructor (config: {host:string,to:string,from:string}) {
        if ((!config.host) ||
            (!config.to) ||
            (!config.from)) {
                throw(function() { console.error('missing smtp config')} )
            }
        else {
            this.conn = mail.createTransport("smtp:"+config.host);
            this.to = config.to;
            this.from = config.from;
        }
    }
    
    public send (data:Mail, cb?) {
        this.conn.sendMail(
            {from:this.from,
             to:this.to,
             subject:"github webhook: "+data.kind ,
             html:data.body},
            function (err,info) {
                if (err) { console.error(err) }
                else {
                    console.log('mail sent',info);
                    if (cb) cb()
                }
            });
    }
}
